### Kërkesa

Në një kompani shefi ka vënë dy roja që numërojnë sa herë hyjnë
punonjësit në ndërtesë. Ndonjëherë rojat mund të dremisin pak, por për
fat të mirë asnjëherë nuk dremisin në të njëjtën kohë, të paktën njëri
qëndron zgjuar për të numëruar ata që hyjnë.

Tani shefi do të dijë sa herë ka hyrë një punonjës në ndërtesë. Ai
pyeti rojat dhe i pari i tha numrin A, kurse i dyti i tha numrin B.
Ndihmojeni shefin të llogarisë minimumin dhe maksimumin e mundshëm të
herëve që ka hyrë punonjësi në ndërtesë.

Referenca: https://www.codechef.com/problems/REMISS

#### Shembull

```
$ cat input.txt
1
19 17

$ python3 prog.py < input.txt
19 36
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    a, b = map(int, input().split())
    print(max(a,b), a+b)
```
https://tinyurl.com/101-prog-009

#### Sqarime

Minimumi i mundshëm është kur hyrjet që ka numëruar njëri i ka
numëruar gjithashtu edhe tjetri. Kështu që minimumi është numri më i
madh prej të dyve.

Maksimumi i mundshëm është kur hyrjet i ka numëruar ose njëri ose
tjetri, por jo të dy njëkohësisht. Kështu që maksimumi është shuma e
dy numrave.

Funksioni `max()` kthen më të madhin e numrave që i jepen.


### Detyra

Një pikë shitje bën zbritje 10% nëse sasia e mallit të blerë është mbi 1000 copë.
Bëni një program që llogarit vlerën e blerjes, duke ditur sasinë dhe çmimin.

Referenca: https://www.codechef.com/problems/FLOW009

#### Shembull

```
$ cat input.txt
3
100 120
10 20
1200 20

$ python3 prog.py < input.txt
12000.000000
200.000000
21600.000000
```
