### Kërkesa

Një makinë llogaritëse është me difekt. Kur mbledh dy numra ajo nuk e
transferon tepricën, p.sh 12 + 9 sipas saj del 11.  Bëni një program
që merr 2 numra dhe nxjerr rezultatin që do nxirrte kjo makinë
llogaritëse për mbledhjen e tyre.

Referenca: https://www.codechef.com/problems/BUGCAL

#### Shembull

```
$ cat input.txt
2
12 9
25 25

$ python3 prog.py < input.txt
11
40
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    a, b = map(int, input().split())
    c = 0
    p = 1
    while a > 0 or b > 0:
        d1 = a % 10
        d2 = b % 10
        d = (d1 + d2) % 10
        c += p * d
        a //= 10
        b //= 10
        p *= 10
    print(c)
```

#### Sqarime

Marrim shifrën e fundit të dy numrave, duke gjetur mbetjen e
pjesëtimit me 10. I mbledhim këto dhe gjejmë mbetjen e pjesëtimit me
10, e cila është njëra nga shifrat e numrit të ri. Të ndryshorja **p**
mbajmë fuqitë e dhjetës (1, 10, 100, 1000, etj.) të cilat tregojnë
edhe se çfarë është kjo shifra e numrit të ri që sapo gjetëm (njëshe,
dhjetëshe, qindëshe, etj.) Kështu që e shumëzojmë shifrën me **p** dhe
ia shtojmë numrit. Ndërkohë marrim edhe herësin e pjesëtimit me dhjetë
për dy numrat **a** dhe **b**, që cikli të vazhdojë me shifrën tjetër.

### Zgjidhja 2

```python
for _ in range(int(input())):
    a, b = map(int, input().split())
    l = []
    while a > 0 or b > 0:
        d1 = a % 10
        d2 = b % 10
        l.append((d1 + d2) % 10)
        a //= 10
        b //= 10
    l.reverse()
    print(''.join(map(str, l)))
```

#### Sqarime

Pak a shumë e njëjta zgjidhje, vetëm se shifrat e numrit të ri mbajmë
në një listë dhe pastaj i bashkojmë si një varg (string).

### Detyra

Në një listë me **N** numra natyrorë mund të shtojmë edhe **K** numra
të tjerë sipas dëshirës, dhe pastaj gjejmë të mesmen e të gjithë
numrave. E mesmja e një liste është elementi që ndodhet në mesin e
listës, pasi ti kemi renditur. P.sh. e mesmja e **[2, 1, 5, 2, 4]**
është **2**, kurse e mesmja e **[3, 3, 1, 3, 3]** është 3.

Gjeni se sa është vlera më e madhe e të mesmes që mund të merret në
këtë mënyrë. Na jepet gjithashtu që **K < N** dhe që **N + K** është
numër tek.

Referenca: https://www.codechef.com/problems/CK87MEDI

#### Shembull

```
$ cat input.txt
3
2 1
4 7
4 3
9 2 8 6
5 2
6 1 1 1 1

$ python3 prog.py < input.txt
7
9
1
```

Na jepen numrat **N** dhe **K**, dhe pastaj N numrat e listës.

Në rastin e parë, një nga zgjidhjet e mundshme është të shtojmë 9,
duke formuar listën [4, 7, 9], e mesmja e të cilës është 7.

Në rastin e tretë, sido që të jenë 2 numrat që do shtohen, e mesmja do
jetë gjithmonë 1.

