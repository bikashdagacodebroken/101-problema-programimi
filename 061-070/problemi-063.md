### Kërkesa

Bëni një program që kontrollon nëse një varg numrash plotëson këto
kushte:
- Ka një qendër (dmth numri i elementeve në të majtë të qendrës është
  i barabartë me numrin e elementeve në të djathtë).
- Numri i parë dhe i fundit është **1**.
- Duke u nisur nga qendra, numrat në të majtë dhe në të djathtë vijnë
  duke zbritur me nga 1.

Referenca: https://www.codechef.com/problems/TEMPLELA

#### Shembull

```
$ cat input.txt
7
5
1 2 3 2 1
7
2 3 4 5 4 3 2
5
1 2 3 4 3
5
1 3 5 3 1
7
1 2 3 4 3 2 1
4
1 2 3 2
4
1 2 2 1

$ python3 prog.py < input.txt
yes
no
no
no
yes
no
no
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    L = list(map(int, input().split()))
    if (n % 2 == 0) or (L[0] != 1) or (L[n-1] != 1):
        print('no')
    else:
        i = 1
        while i <= n // 2:
            if (L[i] != L[i-1] + 1) or (L[n-i] + 1 != L[n-i-1]):
                print('no')
                break
            i += 1
        else:
            print('yes')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    L = list(map(int, input().split()))
    if (n % 2 == 0) or (L[0] != 1) or (L[n-1] != 1):
        print('no')
    else:
        i = 0
        while i < n // 2:
            if (L[i] + 1 != L[i+1]) or (L[n-1 - i] + 1 != L[n-2 - i]):
                print('no')
                break
            i += 1
        else:
            print('yes')
```

### Detyra

Numrat natyrorë vendosen në nyjet e një grafi që ka formën e shinave
të trenit, si në figurë:

![](images/Agha0OoP.png)

Bëni një program që merr dy numra dhe gjen nëse ka brinjë midis këtyre
dy nyjeve.

Referenca: https://www.codechef.com/problems/BRLADDER

#### Shembull

```
$ cat input.txt
7
1 4
4 3
5 4
10 12
1 3
999999999 1000000000
17 2384823

$ python3 prog.py < input.txt
NO
YES
NO
YES
YES
YES
NO
```
