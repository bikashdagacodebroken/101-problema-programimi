### Kërkesa

Bëni një program i cili gjen nëse një listë me numra përfshihet si
nënlistë në një listë tjetër.

#### Shembull

```
$ cat input.txt
4
6
1 2 3 4 5 6
3
2 3 4
6
22 5 6 33 1 4
2
4 15
6
22 5 6 33 1 4
2
1 4
6
22 5 6 33 1 4
2
4 1

$ python3 prog.py < input.txt
Yes
No
Yes
No
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n1 = int(input())
    L1 = list(map(int, input().split()))
    n2 = int(input())
    L2 = list(map(int, input().split()))
    i = 0
    while i < n1-n2+1:
        found = True
        j = 0
        while j < n2:
            if L1[i+j] != L2[j]:
                found = False
                break
            j += 1
        if found:
            break
        i += 1

    if found:
        print("Yes")
    else:
        print("No")
```

#### Sqarime

Për çdo pozicion në listën e parë kontrollojmë nëse ndodhet aty lista
e dytë.  Nëse e gjejmë diku, e mbyllim ciklin sepse nuk është nevoja
të kërkojmë më tej.

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    n1 = int(input())
    L1 = list(map(int, input().split()))
    n2 = int(input())
    L2 = list(map(int, input().split()))
    for i in range(n1-n2+1):
        for j in range(n2):
            if L1[i+j] != L2[j]:
                break
        else:
            print("Yes")
            break
    else:
        print('No')
```

#### Sqarime

E ngjashme me zgjidhjen e parë, por e ndërtuar me `for`, që e bën
programin më kompakt.

### Detyra

Bëni një program i cili gjen nëse numrat e një liste përmbahen në një
listë tjetër.

Referenca: https://www.codechef.com/problems/CHEFSQ

#### Shembull

```
$ cat input.txt
4
6
1 2 3 4 5 6
3
2 3 4
6
22 5 6 33 1 4
2
4 15
6
22 5 6 33 1 4
2
1 4
6
22 5 6 33 1 4
2
4 1

$ python3 prog.py < input.txt
Yes
No
Yes
Yes
```
