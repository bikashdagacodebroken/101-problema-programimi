### Kërkesa

Bëni një program që merr një numër natyror dhe nxjerr **1** nëse numri
ka një shifër, **2** nëse numri ka dy shifra, **3** nëse numri ka tre shifra.
dhe **More than 3 digits** nëse numri ka më shumë se tre shifra.

Referenca: https://www.codechef.com/problems/HOWMANY#

#### Shembull

```
$ cat input.txt
5
9
17
235
62627
31

$ python3 prog.py < input.txt
1
2
3
More than 3 digits
2
```

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    if n < 10:
        print(1)
    elif n < 100:
        print(2)
    elif n < 1000:
        print(3)
    else:
        print('More than 3 digits')
```
