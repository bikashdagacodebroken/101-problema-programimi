### Kërkesa

Bëni një program i cili merr një numër dhe e rrit me 1 nëse ai është i
plotpjesëtueshëm me 4, përndryshe e zvogëlon me 1.

Referenca: https://www.codechef.com/problems/DECINC

#### Shembull

```
$ cat input.txt
2
4
5

$ python3 prog.py < input.txt
5
4
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n = int(input())
    if n % 4 == 0:
        print(n+1)
    else:
        print(n-1)
```
