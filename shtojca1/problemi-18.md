### Kërkesa

Kemi një listë me numra. Duke përdorur shifrat e këtyre numrave mund
të formojmë një numër tjetër. Cili është numri më i madh që mund të
formojmë?

Referenca: https://www.codechef.com/DARG2019/problems/LOALL

#### Shembull

```
$ cat input.txt
2
2 23 1
3 7 11 20

$ python3 prog.py < input.txt
3221
732110
```

### Zgjidhja

```python
for _ in range(int(input())):
    D = []    # list of all digits
    for n in input().split():
        D.extend(list(n))
    D.sort(reverse=True)
    print(*D, sep='')
```

