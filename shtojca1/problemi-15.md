### Kërkesa

Jepet një varg me zero-njësha. Numërojmë kalimet 0-1 ose 1-0 në këtë
varg, duke e menduar vargun si ciklik (shifrën e fundit ta mendojmë si
të ngjitur me shifrën e parë). Këtë varg e quajmë *uniform* nëse ka të
shumtën 2 kalime të tilla, përndryshe e quajmë *jo-uniform*. Gjeni
nëse vargu i dhënë është uniform ose jo.

Referenca: https://www.codechef.com/problems/STRLBP

#### Shembull

```
$ cat input.txt
4
00000000
10101010
10000001
10010011

$ python3 prog.py < input.txt
uniform
non-uniform
uniform
non-uniform
```

### Zgjidhja

```python
for _ in range(int(input())):
    V = input()
    nr = 0
    for i in range(len(V) - 1):
        if V[i] != V[i+1]:
            nr += 1
    if V[0] != V[-1]:
        nr += 1
    print('non-uniform') if nr > 2 else print('uniform')
```
