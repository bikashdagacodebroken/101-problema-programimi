### Kërkesa

Në një restorant shkojnë shumë programues. Ata i lënë shënimet e tyre
në librin e përshtypjeve në formatin binar (me zero dhe njësha). Por
shefi i restorantit nuk ka haber nga formati binar, kështu që i ka
dhënë karar që një përshtypje ta quajë pozitive nëse përmban vargun
**010** ose **101**, dhe ta quajë negative në të kundërt.

Bëni një program që merr përshtypjet në formatin me zero njësha dhe
gjen nëse janë të mira ose të këqija.

Referenca: https://www.codechef.com/problems/ERROR

#### Shembull

```
$ cat input.txt
2
11111110
10101010101010

$ python3 prog.py < input.txt
Bad
Good
```

Rasti i parë nuk përmban asnjë nënvarg 010 ose 101. Rasti i dytë i
përmban të dyja.

### Zgjidhja 1

```python
for _ in range(int(input())):
    p = input()
    for i in range(len(p) - 2):
        if p[i]=='1' and p[i+1]=='0' and p[i+2]=='1':
            print('Good')
            break
        if p[i]=='0' and p[i+1]=='1' and p[i+2]=='0':
            print('Good')
            break
    else:
        print('Bad')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    p = input()
    for i in range(len(p) - 2):
        if p[i:i+3]=='101' or p[i:i+3]=='010':
            print('Good')
            break
    else:
        print('Bad')
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    p = input()
    if '101' in p or '010' in p:
        print('Good')
    else:
        print('Bad')
```
