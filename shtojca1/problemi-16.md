### Kërkesa

Jepen kordinatat e tre pikave në një plan: $`(x_1, y_1), (x_2, y_2),
(x_3, y_3)`$.  Gjeni nëse këto pika janë kulme të një trekëndëshi
kënddrejtë.

Referenca: https://www.codechef.com/problems/RIGHTRI

#### Shembull

```
$ cat input.txt
5
0 5 19 5 0 0
17 19 12 16 19 0
5 14 6 13 8 7
0 4 0 14 3 14
0 2 0 14 9 2

$ python3 prog.py < input.txt
yes
no
no
yes
yes
```

Janë dhënë numrat x1, y1, x2, y2, x3, y3.

### Zgjidhja 1

```python
for _ in range(int(input())):
    x1, y1, x2, y2, x3, y3 = map(int, input().split())
    d1 = (x1 - x2)**2 + (y1 - y2)**2
    d2 = (x1 - x3)**2 + (y1 - y3)**2
    d3 = (x2 - x3)**2 + (y2 - y3)**2
    if max(d1, d2, d3) * 2 == d1 + d2 + d3:
        print('yes')
    else:
        print('no')
```

#### Sqarime

Përdorim teoremën e Pitagorës ($`a^2 + b^2 = c^2`$).

### Zgjidhja 2

```python
for _ in range(int(input())):
    x1, y1, x2, y2, x3, y3 = map(int, input().split())
    p1 = (x1 - x2)*(x1 - x3) + (y1 - y2)*(y1 - y3)
    p2 = (x1 - x2)*(x2 - x3) + (y1 - y2)*(y2 - y3)
    p3 = (x1 - x3)*(x2 - x3) + (y1 - y3)*(y2 - y3)
    if p1*p2*p3 == 0:
        print('yes')
    else:
        print('no')
```

#### Sqarime

Përdorim faktin që prodhimi i dy vektorëve pingulë është zero.
