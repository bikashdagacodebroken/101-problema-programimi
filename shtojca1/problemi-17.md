### Kërkesa

Ti ke shoqëri me Anin dhe Benin dhe shkëmben mesazhe me ta. Ani
shkruan gjithmonë me shkronja të vogla, kurse Beni shkruan gjithmonë
me shkronja të mëdha.

Sapo të ka ardhur një mesazh që ka edhe shkronja të vogla edhe të
mëdha. Në fakt Ani dhe Beni mund të bëjnë ndonjëherë edhe gabime gjatë
shkrimit, dhe në vend të një shkronje të vogël të bëjnë një të madhe,
ose anasjelltas. Por jo më shumë se **K** të tilla në një mesazh.

Gjeni nëse këtë mesazh mund ta ketë shkruar Ani, ose Beni, ose të dy,
ose asnjëri.

Referenca: https://www.codechef.com/problems/GOODBAD

#### Shembull

```
$ cat input.txt
4
5 1
frauD
5 1
FRAUD
4 4
Life
10 4
sTRAWBerry

$ python3 prog.py < input.txt
Ani
Beni
Secili
Asnjeri
```

1. Mund të ketë vetëm 1 gabim, kështu që këtë mesazh duhet ta ketë
   shkruar Ani (por shkronja 'd' gabimisht i ka dalë e madhe). Nuk
   mund ta ketë shkruar Beni, sepse në këtë rast Beni do kishte bërë 4
   gabime (gjë që nuk ka mundësi).
   
1. Mund të ketë vetëm 1 gabim, kështu që duhet ta ketë shkruar
   Beni. Po ta kishte shkruar Ani do kishte bërë 5 gabime (gjë që s'ka
   mundësi).
   
1. Meqenëse janë 4 gabime të mundshme, atere mund ta ketë shkruar edhe
   Ani edhe Beni dhe secili të ketë bërë gabime (Ani 1 ose Beni 3).
   
1. Janë 4 gabime të mundshme, por nqs do ta kishte shkruar Ani do
   kishte bërë 5 gabime, dhe po ta kishte shkruar Beni do kishte bërë
   5 gabime.  Kështu që asnjëri prej tyre nuk mund ta ketë dërguar
   këtë mesazh.

### Zgjidhja 1

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    S = input()
    l = u = 0
    for c in S:
        if c.islower():
            l += 1
        else:
            u += 1
    if l <= k and u <= k:
        print('Secili')
    elif l > k and u > k:
        print('Asnjeri')
    elif l <= k and u > k:
        print('Ani')
    else: # l > k and u <= k
        print('Beni')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    S = input()
    l = sum([1 for c in S if c.islower()])
    u = n - l
    if l <= k and u <= k:
        print('Secili')
    elif l > k and u > k:
        print('Asnjeri')
    elif l <= k and u > k:
        print('Ani')
    else: # l > k and u <= k
        print('Beni')
```
