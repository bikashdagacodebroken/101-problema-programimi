### Kërkesa

Bëni një program që merr një shkronjë të alfabetit anglisht, dhe
nxjerr "Vowel" nëse është zanore, ose "Consonant" nëse është
bashkëtingëllore.

Zanoret në alfabetin anglisht janë: 'A', 'E', 'I', 'O', 'U'.

Referenca: https://www.codechef.com/problems/VOWELTB

#### Shembull

```
$ cat input.txt
2
Z
A

$ python3 prog.py < input.txt
Consonant
Vowel
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    c = input()
    if c == 'A' or c == 'E' or c == 'I' or c == 'O' or c == 'U':
        print('Vowel')
    else:
        print('Consonant')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    c = input()
    if c in ['A', 'E', 'I', 'O', 'U']:
        print('Vowel')
    else:
        print('Consonant')
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    c = input()
    if c in 'AEIOU':
        print('Vowel')
    else:
        print('Consonant')
```

### Zgjidhja 4

```python
for _ in range(int(input())):
    print('Vowel') if input() in 'AEIOU' else print('Consonant')
```
