### Kërkesa

Bëni një program i cili nxjerr diferencën e dy numrave të dhënë, nëse
i pari është më i madh se i dyti, ose shumën e tyre në të kundërt.

Referenca: https://www.codechef.com/problems/DIFFSUM

#### Shembull

```
$ cat input.txt
82
28

$ python3 prog.py < input.txt
54
```

### Zgjidhja

```python
n1 = int(input())
n2 = int(input())
if n1 > n2:
    print(n1 - n2)
else:
    print(n1 + n2)
```
