### Kërkesa

Një numër të dhënë **X** e shumëzojmë me 2 deri sa të bëhet i
pjesëtueshëm me 10. Gjeni numrin më të vogël të shumëzimeve deri sa të
plotësohet ky kusht, ose përcaktoni që kjo është e pamundur.

Referenca: https://www.codechef.com/problems/TWOVSTEN

#### Shembull

```
$ cat input.txt
3
10
25
1

$ python3 prog.py < input.txt
0
1
-1
```

Kur është e pamundur e shënojmë përgjigjen  -1.

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    if n % 10 == 0:
        print(0)
    elif n % 5 == 0:
        print(1)
    else:
        print(-1)
```
