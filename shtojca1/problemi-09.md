### Kërkesa

Në një olimpiadë janë **P** pjesëmarrës. Nqs një problem zgjidhet nga
më shumë se gjysma e pjesëmarrësve, ky problem quhet i thjeshtë. Nqs
një problem zgjidhet nga e shumta **P/10** pjesëmarrës, quhet i
vështirë. Teza e olimpiadës quhet e balancuar nëse ka vetëm një problem
të thjeshtë dhe vetëm 2 të vështirë.

Bëni një program që merr numrin e pjesëmarrësve që kanë zgjidhur
secilin problem të olimpiadës, dhe nxjerr nëse teza e olimpiadës ka
qenë e balancuar ose jo.

Referenca: https://www.codechef.com/problems/PERFCONT

#### Shembull

```
$ cat input.txt
6
3 100
10 1 100
3 100
11 1 100
3 100
10 1 10
3 100
10 1 50
4 100
50 50 50 50
4 100
1 1 1 1

$ python3 prog.py < input.txt
yes
no
no
yes
no
no
```

1. Janë dy problema të vështira dhe një i lehtë, kështu që është e balancuar
1. Problemi i dytë është i vështirë dhe i treti është i lehtë. I pari
   nuk është as i lehtë as i vështirë. Teza nuk është e balancuar.
1. Tre problema të vështira. Jo e balancuar.
1. Dy të parat janë të vështira, i treti është i lehtë. E balancuar.
1. Katër problema të lehta. Jo e balancuar.
1. Katër problema të vështira. Jo e balancuar.

### Zgjidhja

```python
for _ in range(int(input())):
    n, p = map(int, input().split())
    L = list(map(int, input().split()))
    n1 = 0    # problemat e lehta
    n2 = 0    # problemat e vështira
    for i in L:
        if i >= p // 2:
            n1 += 1
        if i <= p // 10:
            n2 += 1
    if n1 == 1 and n2 == 2:
        print('yes')
    else:
        print('no')
```

