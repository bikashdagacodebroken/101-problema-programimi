### Kërkesa

Një universitet përdor sistemin e vlerësimit me 5 pikë. Në një provim
studentët mund të vlerësohen me pikët nga 2 (që është ngelës) deri në
5 (që është shkëlqyer). Universiteti u jep bursa studentëve nqs plotësojnë
këto kushte:
- Nuk janë ngelës në asnjë lëndë.
- Kanë të paktën një lëndë ku janë të shkëlqyer.
- Mesataren e të gjitha lëndëve e kanë jo më pak se **4.0**

Nqs jepen vlerësimet e studentëve në të gjitha provimet, bëni një
program që gjen se cilët prej tyre përfitojnë bursa.

Referenca: https://www.codechef.com/problems/EGRANDR

#### Shembull

```
$ cat input.txt
2
5
3 5 4 4 3
5
3 4 4 4 5

$ python3 prog.py < input.txt
No
Yes
```

Studenti i parë e ka mesataren 3.8, kështu që nuk përfiton bursë.

Studenti i dytë i plotëson të gjitha kushtet, kështu që përfiton
bursë.

### Zgjidhja 1

```python
t = int(input())
while t > 0:
    t -= 1
    n = int(input())
    P = [int(p) for p in input().split()]
    has_2 = False
    has_5 = False
    s = 0
    for p in P:
        if p == 2:
            has_2 = True
        if p == 5:
            has_5 = True
        s += p
    if s/n >= 4.0 and has_5 and not has_2:
        print('Yes')
    else:
        print('No')
```

### Zgjidhja 2

```python
t = int(input())
while t > 0:
    t -= 1
    n = int(input())
    P = [int(p) for p in input().split()]
    if (sum(P) / n >= 4.0) and (5 in P) and (not 2 in P):
        print('Yes')
    else:
        print('No')
```

