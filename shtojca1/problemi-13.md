### Kërkesa

Bëni një program që merr dy numra të plotë dhe një veprim aritmetik
dhe nxjerr rezultatin.

Referenca: https://www.codechef.com/problems/URCALC

#### Shembull

```
$ cat input.txt
4
8 / 2
5 + 3
7 * 5
1 - 9

$ python3 prog.py < input.txt
4.0
8
35
-8
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    a, op, b = input().split()
    a = int(a)
    b = int(b)
    if op == '+':
        r = a + b
    elif op == '-':
        r = a - b
    elif op == '*':
        r = a * b
    elif op == '/':
        r = a / b
    print(r)
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    print(eval(input()))
```
