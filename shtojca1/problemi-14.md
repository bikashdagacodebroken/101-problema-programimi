### Kërkesa

Në një konkurs Alisa dhe Bobi bëjnë **N** gara vrapimi, ku për çdo
**i** nga 1 në N, Alisa e mbaron garën **i** në kohën $`A_i`$ minuta,
kurse Bobi e mbaron në kohën $`B_i`$ minuta. Konkursin e fiton ai që
ka shumën e kohëve më të vogël. Sipas rregullave, Alisa mund të heqë
nga llogaritja njërën prej garave, kë të dojë. Po ashtu dhe Bobi.
Gjeni se cili prej tyre e fiton konkursin. Nqs shuma e kohëve del e
barabartë, atere janë barazim.

Referenca: https://www.codechef.com/problems/CO92JUDG

#### Shembull

```
$ cat input.txt
3
5
3 1 3 3 4
1 6 2 5 3
5
1 6 2 5 3
3 1 3 3 4
3
4 1 3
2 2 7

$ python3 prog.py < input.txt
Alice
Bob
Draw
```

Në rastin e parë kemi 5 gara. Koha e Alisës është 3 + 1 + 3 + 3 + 0
= 10. Koha e Bobit është 1 + 0 + 2 + 5 + 3 = 11. Fiton Alisa.

Në rastin e tretë kemi 0 + 1 + 3 = 4 kohën e Alisës dhe 2 + 2 + 0 = 4
kohën e Bobit, kështu që dalin barazim.

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    A = [int(i) for i in input().split()]
    B = [int(i) for i in input().split()]
    s1 = 0
    m1 = 0
    for a in A:
        s1 += a
        if a > m1:
            m1 = a
    s2 = 0
    m2 = 0
    for b in B:
        s2 += b
        if b > m2:
            m2 = b
    if s1 - m1 < s2 - m2:
        print('Alice')
    elif s1 - m1 > s2 - m2:
        print('Bob')
    else:
        print('Draw')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    input()
    A = [int(i) for i in input().split()]
    B = [int(i) for i in input().split()]
    d = (sum(A) - max(A)) - (sum(B) - max(B))
    if d < 0:
        print('Alice')
    elif d > 0:
        print('Bob')
    else:
        print('Draw')
```
