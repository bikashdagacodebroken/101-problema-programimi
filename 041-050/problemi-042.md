### Kërkesa

Igori i vogël i ka qejf filmat. Këtë fundjavë janë **n** filma që mund
të shihen, ku secili film ka gjatësi $`L_i`$ nga 1 deri në 100
minuta, dhe vlerësim $`R_i`$ nga 1 deri në 100 pikë. Por Igori ka
mundësi të shohë vetëm 1 folëm, kështu që ai dëshiron të zgjedhë atë
folëm që ka vlerë maksimale për $`L_i*R_i`$. Nëse janë disa filma të
tillë, atëherë ai do zgjidhte atë që ka vlerën më të madhe të
$`R_i`$. Nëse prapë janë disa të tillë, atëherë do të zgjidhte atë që
është i pari në listë. Bëni një program për ta ndihmuar Igorin të
zgjedhë një film.

Referenca: https://www.codechef.com/problems/MOVIEWKN

#### Shembull

```
$ cat input.txt
2
2
1 2
2 1
4
2 1 4 1
2 4 1 4

$ python3 prog.py < input.txt
1
2
```

Rreshti i parë jep vlerat **L**, rreshti i dytë jep vlerat **R**.

Në rastin e parë të dy filmat kanë të njëjtën vlerë të $`L*R`$, por
filmi i parë ka vlerësim (R) më të mirë.

Në rastin e dytë, filmi i dytë dhe i katërt janë njësoj të mirë,
kështu që kemi zgjedhur të parin që është në listë.

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n = int(input())
    L = list(map(int, input().split()))
    R = list(map(int, input().split()))
    v_max = 0
    r_max = 0
    i_max = 0
    for i in range(n):
        l = L[i]
        r = R[i]
        v = l * r
        if v > v_max:
            v_max = v
            r_max = r
            i_max = i
        elif v == v_max and r > r_max:
            v_max = v
            r_max = r
            i_max = i
    print(i_max + 1)
```

### Detyra

Pas një kohe të gjatë, Cufi vendosi më në fund ta rinovojë shtëpinë.
Shtëpia e Cufit ka **N** dhoma. Secila prej dhomave është e lyer me
një nga ngjyrat e kuqe, blu dhe jeshile.  Konfigurimi i ngjyrave të
shtëpisë na jepet me anë të një vargu me shkronjat **R**, **B**,
**G**. Cufi do që ta lyejë shtëpinë në mënyrë që të gjitha dhomat të
kenë të njëjtën ngjyrë, por ngaqë është pak dembel, do që të lyejë sa
më pak dhoma (mundësisht asnjë). Bëni një program që gjen numrin më të
vogël të dhomave që duhet të lyejë Cufi.

Referenca: https://www.codechef.com/problems/COLOR

#### Shembull

```
$ cat input.txt
3
3
RGR
3
RRR
3
RGB

$ python3 prog.py < input.txt
1
0
2
```
