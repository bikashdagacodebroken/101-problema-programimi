### Kërkesa

Bëni një program i cili gjen nëse numrat e një liste përmbahen në një
listë tjetër.

Referenca: https://www.codechef.com/problems/CHEFSQ

#### Shembull

```
$ cat input.txt
4
6
1 2 3 4 5 6
3
2 3 4
6
22 5 6 33 1 4
2
4 15
6
22 5 6 33 1 4
2
1 4
6
22 5 6 33 1 4
2
4 1

$ python3 prog.py < input.txt
Yes
No
Yes
Yes
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n1 = int(input())
    L1 = list(map(int, input().split()))
    n2 = int(input())
    L2 = list(map(int, input().split()))
    for n in L2:
        if n not in L1:
            print('No')
            break
    else:
        print('Yes')
```

#### Sqarime

Për çdo element të listës së dytë kontrollojmë nëse ndodhet në listën
e parë.

### Zgjidhja 2

```python
for _ in range(int(input())):
    input()
    S1 = set(map(int, input().split()))
    input()
    S2 = set(map(int, input().split()))
    print('Yes') if S2.issubset(S1) else print('No')
```

#### Sqarime

Duke i trajtuar listat e dhëna të numrave si bashkësi, kontrollojmë
nëse bashkësia e dytë është nënbashkësi e të parës (me anë të
funksionit `issubset()`)

### Detyra

Dejvi ka disa shokë të çuditshëm. Me rastin e ditëlindjes së tij
secili prej tyre i ka kërkuar që ta qerasë në një datë të caktuar, me
kushtin që do ta prishë shoqërinë në rast se nuk e qeras pikërisht në
atë datë. Mirëpo Dejvi nuk mund të qerasë më shumë se një prej tyre në
një datë të caktuar. Gjeni se me sa prej tyre mund ta ruajë shoqërinë.

Referenca: https://www.codechef.com/problems/CFRTEST

#### Shembull

```
$ cat input.txt
2
2
3 2
2
1 1

$ python3 prog.py < input.txt
2
1
```

Në rastin e parë, të dy shokët e kërkojnë qerasjen në data të
ndryshme, kështu që mund ta ruajë shoqërinë me të dy. Në rastin e
dytë, të dy e kërkojnë qerasjen në të njëjtën ditë, kështu që mund ta
ruajë shoqërinë vetëm me njërin prej tyre.
