### Kërkesa

Andi ka mbledhur **A** kokrra manaferrash kurse Beni **B**. Ata po
bëjnë një lojë të tillë: Andi ha 1 kokërr, pastaj Beni 2, pastaj Andi 3,
e kështu me radhë. I pari që nuk i kanë mbetur për të ngrënë aq
kokrra sa duhet e humbet lojën. Bëni një program që gjen se kush prej
tyre e fiton lojën.

Referenca: https://www.codechef.com/problems/CANDY123

#### Shembull

```
$ cat input.txt
10
3 2
4 2
1 1
1 2
1 3
9 3
9 11
9 12
9 1000
8 11

$ python3 prog.py < input.txt
Beni
Andi
Andi
Beni
Beni
Andi
Andi
Beni
Beni
Beni
```

Në rastin e parë Andi ha 1, Beni 2, dhe Andi nuk mund të hajë 3,
kështu që lojën e fiton Beni.

Në rastin e shtatë, Andi ha 1, Beni 2, Andi 3, Beni 4, Andi 5, dhe
Beni nuk ka 6 kokrra, kështu që fiton Andi.


### Zgjidhja

```python
T = int(input())
for t in range(T):
    a, b = map(int, input().split())
    i = 0
    while True:
        i += 1
        if a < i:
            print('Beni')
            break
        else:
            a -= i
        i += 1
        if b < i:
            print('Andi')
            break
        else:
            b -= i
```

#### Sqarime

Kemi një cikël të pafund i cili ndërpritet kur njëri nga lojtarët e
humb lojën, ndërkohë që ne printojmë si fitues emrin e lojtarit
tjetër.

### Detyra

Na jepen dy vargje shkronjash **A** dhe **B**. A është e mundur të
gjejmë një nënvarg **s1** në **A** dhe një nënvarg **s2** në **B**, që
mos të jenë bosh, dhe që **s1+s2** të jetë palindromë?

Shenja **+** tregon bashkimin ose ngjitjen e dy vargjeve, kurse
palindromë do të thotë që po ta lexosh vargun nga fundi në fillim
është njësoj me vargun fillestar (nga fillimi në fund).

Referenca: https://www.codechef.com/problems/STRPALIN

#### Shembull

```
$ cat input.txt
3
abc
abc
a
b
abba
baab

$ python3 prog.py < input.txt
Yes
No
Yes
```

Në rastin e parë, një nga zgjedhjet e mundshme është `s1 = "ab"` dhe
`s2 = "a"`. Atere kemi `s1+s2 = "aba"` e cila është palindromë, kështu
që përgjigja është "Yes".

Në rastin e dytë kemi `A = "a"` dhe `B = "b"` dhe nuk ka asnjë mundësi
që të krijojmë ndonjë palindromë, kështu që përgjigja është "No".
