### Kërkesa

Kemi një varg me numra ku të gjithë numrat përsëriten një numër çift
herësh, me përjashtim të njërit i cili përsëritet një numër tek
herësh. Të gjendet ky numër.

Referenca: https://www.codechef.com/problems/MISSP

#### Shembull

```
$ cat input.txt
2
3
1
2
1
5
1
1
2
2
3

$ python3 prog.py < input.txt
2
3
```

Janë 2 raste testimi, i pari ka 3 numra (njëri nën tjetrin), dhe i dyti ka 5 numra.

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n = int(input())
    l = []
    for i in range(n):
        l.append(int(input()))
    l.sort()
    l.append(None)
    i = 0
    while i < n:
        if l[i] != l[i+1]:
            break
        i += 2
    print(l[i])
```

#### Sqarime

Krijojmë një listë dhe i fusim në të të gjithë numrat. Pastaj listën e
rendisim. Në këtë mënyrë, të gjithë numrat e barabartë i kemi përkrah
njëri-tjetrit dhe mund ti kalojmë dy e nga dy. Aty ku dy numrat e
radhës nuk janë të barabartë, i bie që të jetë numri që përsëritet
një numër tek herësh. Për të kapur edhe rastin kur ky numër ndodhet
në fund të listës (së renditur), kemi shtuar në list edhe një element
që është i ndryshëm nga gjithë numrat e tjerë: `None`.

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    n = int(input())
    l = []
    for i in range(n):
        a = input()
        if a in l:
            l.remove(a)
        else:
            l.append(a)
    print(l[0])
```

#### Sqarime

Krijojmë një listë bosh dhe çdo numër që lexojmë ose e shtojmë në
listë nëse nuk është në të, ose e heqim nga lista nëse është në të.
Në këtë mënyrë, në fund të këtij procesi do na ketë mbetur në listë
vetëm ai numër që përsëritet një numër tek herësh.

### Detyra

Në brinjët e një trekëndëshi barabrinjës vendosim disa pika të
baraslarguara nga njëra-tjetra, dhe i bashkojmë me vija, si në figurë:

![](images/Agha0OoP.png)

Le të quajmë të rregullt vetëm trekëndëshat që e kanë majën lart dhe
bazën poshtë. Nqs ndarjet e brinjëve i quajmë 1 njësi, dhe trekëndëshi
i madh e ka brinjën $`L`$ njësi, sa trekëndësha të rregullt me brinjë
$`K`$ njësi (ku $`1 \leq K \leq N`$) ndodhen në figurë?

Referenca: https://www.codechef.com/problems/ZUBTRCNT

#### Shembull

```
$ cat input.txt
2
4 3
4 4

$ python3 prog.py < input.txt
Case 1: 3
Case 2: 1
```

Rezultati duhet të shfaqet në formën `Case i: ` dhe pastaj përgjigja.
