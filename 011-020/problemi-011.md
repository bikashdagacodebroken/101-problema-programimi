### Kërkesa

Bëni një program që merr kodin e një anije (si një shkronjë) dhe shfaq
llojin e saj (si emër të plotë). Kodet dhe llojet e anijeve janë si më
poshtë:

| **Kodi** | **Lloji**  |
| :------: | :--------- |
| B ose b  | BattleShip |
| C ose c  | Cruiser    |
| D ose d  | Destroyer  |
| F ose f  | Frigate    |

Referenca: https://www.codechef.com/problems/FLOW010

#### Shembull

```
$ cat input.txt
3
B
c
D

$ python3 prog.py < input.txt
BattleShip
Cruiser
Destroyer
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    k = input()
    if k == 'b' or k == 'B':
        print('BattleShip')
    elif k == 'c' or k == 'C':
        print('Cruiser')
    elif k == 'd' or k == 'D':
        print('Destroyer')
    else:
        print('Frigate')
```

### Zgjidhja 2

```python
ships = {
    'b': 'BattleShip',
    'c': 'Cruiser',
    'd': 'Destroyer',
    'f': 'Frigate'
}
for _ in range(int(input())):
    print(ships[input().lower()])
```

#### Sqarime

Funksioni `lower()` që aplikohet te një varg, e kthen këtë varg në
shkronja të vogla. Kurse `ships` është një strukturë që quhet *fjalor*
(*dictionary*). Provoni këto komanda në promptin e Python:
```
$ python3
>>> fruits = { 'a': 'Apple', 'b': 'Banana' }
>>> fruits['b']
'Banana'
>>> fruits.get('a')
'Apple'
>>> fruits['o'] = 'Orange'
>>> fruits.get('o')
'Orange'
>>> fruits['x']
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'x'
>>> fruits.get('x')
>>> fruits.get('x', False)
False
>>> 
```

### Detyra

Kur pohojmë zakonisht e tundim kokën nga lart poshtë. Le ta shënojmë
me `Y`, për "yes".  Kur mohojmë e tundim kokën nga e majta në të
djathtë, rreth boshtit vertikal. Le ta shënojmë me `N` për "no".

Por indianët zakonisht përdorin një shenjë tjetër për të pohuar, duke
e tundur kokën nga e majta në të djathtë rreth boshtit para-mbrapa. Le
ta shënojmë këtë me `I`.

Një djalë vëzhgoi disa persona në një stacion treni dhe mbajti shënim
shenjat që bënin me kokë (duke përdorur shkronjat `Y`, `N` dhe `I`).
Bëni një program që shfaq nëse personi në fjalë është indian, jo
indian, ose nuk mund ta themi me siguri.

Referenca: https://www.codechef.com/problems/HEADBOB

#### Shembull

```
$ cat input.txt
3
5
NNNYY
6
NNINNI
4
NNNN

$ python3 prog.py < input.txt
NOT INDIAN
INDIAN
NOT SURE
```
