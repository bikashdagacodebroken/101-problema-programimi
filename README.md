# 101 Ushtrime Dhe Problema Programimi

Ky libër është për fillestarët në programim, qofshin nxënës të
shkollës 9-vjeçare ose të shkollës së mesme, apo të tjerë të çdo
moshe. Konceptet e programimit mësohen hap pas hapi, nëpërmjet
zgjidhjes së ushtrimeve dhe problemave, të cilat fillojnë shumë të
thjeshta dhe vijnë gradualisht duke u bërë më interesante.

Në fakt, synimi i këtij libri është që ti aftësojë lexuesit në
zgjidhjen e problemave të programimit, dhe jo që t'ju mësojë ndonjë
gjuhë të veçantë programimi. Gjuha e programimit mësohet vetvetiu si
një mjet i nevojshëm për të shprehur zgjidhjen e problemit. Kështu që
mësimi i veçorive të gjuhës së programimit nuk do të jetë shterues, do
të mësojmë vetëm aq sa na duhet.

Po ashtu, edhe mësimi i teknikave algoritmike nuk do të jetë shterues,
do të mësohen vetëm ato që na duhen për problemat përkatëse. Kurse
vetë problemat mund të jenë shpeshherë sfiduese dhe të kërkojnë disa
njohuri matematike dhe aftësi të forta llogjike për tu zgjidhur. Në
fakt, shumica e këtyre problemave janë marrë prej olimpiadave të
programimit që zhvillohen online, dhe shpresoj se do të jenë
zbavitëse, por njëkohësisht edhe do ti ndihmojnë studentët që të
aftësohen për të konkuruar në këto olimpiada. Ose të paktën do ti
ndihmojnë në hapat e para në këtë drejtim.

Gjuha e përdorur për zgjidhjen e problemave është Python (ose më saktë
Python3), si një gjuhë e thjeshtë për fillestarët, me aftësi shprehëse
të fuqishme, e një niveli pak më të lartë se gjuhët e tjera të nivelit
të lartë, por edhe që përdoret gjerësisht në praktikë, në çdo fushë të
programimit, duke përfshirë aplikacionet desktop, aplikacionet
shkencore, aplikacionet web, inteligjencën artificiale, robotikën,
etj. Në fakt, Python është gjuha që përdoret nga Google për motorin e
tij të famshëm të kërkimit, i cili sistemon informacionin e gjithë
botës, për YouTube, etj. A doni më për Python?

Si një mjedis pune për fillestarët, unë sugjeroj që të instalohet
LinuxMint Xfce në një makinë virtuale (VirtualBox), i cili e ka të
instaluar vetvetiu Python3 dhe çdo gjë tjetër që mund të na duhet.
Lidhjet dhe videot e mëposhtme mund t'ju ndihmojnë për ta bërë këtë:
- [Download Linux Mint](https://www.linuxmint.com/download.php)
- [How to Install LinuxMint in VirtualBox](https://www.youtube.com/watch?v=6l-QEeIsecE)
- [How to Dual-Boot Windows10 and LinuxMint](https://www.youtube.com/watch?v=V1XqHFgpqxc)
- [How to Create a Bootable USB for Installing LinuxMint](https://www.youtube.com/watch?v=vSBwjd3CmeA)

Edhe pse nuk është e nevojshme që të studiohet ndonjë tutorial ose
manual për Python para se të fillohet ky libër, po i jap gjithsesi
disa referenca për ata që dëshirojnë të thellohen më tepër në detajet
e gjuhës Python, para ose pasi ta kenë lexuar këtë libër:
- [learnpython.org](http://www.learnpython.org/)
- [Learn Python Programming](https://www.programiz.com/python-programming)
- [Python Programming Tutorial](https://www.youtube.com/watch?v=HBxCHonP6Ro&list=PL6gx4Cwl9DGAcbMi1sH6oAMk4JHw91mC_)
- [How to Think Like a Computer Scientist](http://interactivepython.org/runestone/static/thinkcspy/index.html)

Na vaftë mbarë!
