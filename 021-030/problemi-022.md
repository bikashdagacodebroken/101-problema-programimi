### Kërkesa

Në një lojë të rregullt tenisi, lojtari që shërben ndërrohet çdo 2
pikë, pavarësisht se kush i shënon.  Chef dhe Cook po e luajnë lojën
pak më ndryshe. Ata vendosën që lojtari i shërbimit të ndërrohet çdo
$`k`$ pikë, pavarësisht se kush i shënon.

Në fillim të lojës, radhën e shërbimit e ka gjithmonë Chef-i. Nqs
dimë që deri tani Chef ka shënuar $`p_1`$ pikë dhe Cook ka shënuar
$`p_2`$ pikë, kush e ka tani radhën e shërbimit?

Referenca: https://www.codechef.com/problems/CHSERVE

#### Shembull

```
$ cat input.txt
3
1 3 2
0 3 2
34 55 2

$ python3 prog.py < input.txt
CHEF
COOK
CHEF
```

Tre numrat që jepen janë $`p_1`$, $`p_2`$ dhe $`k`$.

### Zgjidhja

```python
T = int(input())
for t in range(T):
    p1, p2, k = map(int, input().split())
    piket = p1 + p2 + 1
    seti = piket // k
    if piket % k > 0:
        seti += 1
    if seti % 2 == 0:
        print('COOK')
    else:
        print('CHEF')
```

#### Sqarime

Nqs një grup prej $`k`$ pikësh të njëpasnjëshme (ku shërben i njëjti
lojtar) i quajmë një set, atere gjejmë në fillim se për cilin set po
luhet kjo pikë. Nqs seti është tek, atere shërbimin e ka lojtari i
parë, përndryshe e ka lojtari i dytë.

### Detyra

Një qenush gjeti një arkë me N monedha. Ai nuk e hap dot vetë, por
mund të lehë që tu tërheq vëmendjen njerëzve që ndodhen aty pranë. Ai
e di që njerëzit do marrin secili një sasi të barabartë monedhash, dhe
të tjerat do ti lënë në shesh, kështu që ai mund ti marrë.

Qenushi mund ta ndryshojë fortësinë e të lehurit, në mënyrë që ti
tërheqë vëmendjen 1 personi, ose 2 personave, e kështu me radhë, deri
në K persona. Sa persona duhet të thërrasë, në mënyrë që në fund ti
ngelen sa më shumë monedha?

Referenca: https://www.codechef.com/problems/GDOG

#### Shembull

```
$ cat input.txt
2
5 2
11 3

$ python3 prog.py < input.txt
1
2
```
