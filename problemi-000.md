
### Kërkesa

Shkruani një program që merr emrin, mbiemrin, dhe qytetin ku jetoni, dhe nxjerr në ekran:
```
Ju jeni <emri> <mbiemri> nga <qyteti>.
```

### Zgjidhja 1

```python
# lexojmë nga tastiera emrin, mbiemrin dhe qytetin
print('Emri: ', end='')
emri = input()
print('Mbiemri: ', end='')
mbiemri = input()
print('Qyteti: ', end='')
qyteti = input()

# nxjerrim në ekran fjalinë e kërkuar
print('Ju jeni', emri, mbiemri, 'nga', qyteti)
```

https://tinyurl.com/101-prog-000

#### Sqarime

Funksioni `print()` përdoret për të nxjerrë diçka (në ekran), kurse
funksioni `input()` për të lexuar diçka (nga tastiera). Parametri
`end=''` i tregon funksionit `print()` që në fund të mos printojë
asgjë, sepse zakonisht ai printon edhe një *fund-rreshti* (*EOL* --
End Of Line), i cili bën që kursori të kaloj në rreshtin më
poshtë. Kur i japim funksionit `print()` disa parametra, ai i printon
ato me nga një vend bosh në mes.

Hapni terminalin dhe shkruajeni programin brenda direktorisë `prog/p000`:
```
mkdir -p prog/p000
cd prog/p000/
nano p000.py
```

Komanda `mkdir` krijon një direktori të re (`make directory`), kurse
komanda `cd` ndryshon direktorinë e punës (`change directory`).

Në editorin `nano` mund të përdorni kombinimin e tasteve `Ctrl+O` për
ta ruajtur programin, dhe `Ctrl+X` për të dalë. Në vend të editorit
`nano` mund të përdorni edhe *Text Editor* ose ndonjë editor tjetër.

Zbatojeni këtë program nga terminali duke përdorur komandën:
```
python3 p000.py
```

### Zgjidhja 2

```python
# lexojmë emrin, mbiemrin dhe qytetin
emri = input()
mbiemri = input()
qyteti = input()

# nxjerrim në ekran fjalinë e kërkuar
print('Ju jeni {} {} nga {}.'.format(emri, mbiemri, qyteti))
```

https://tinyurl.com/101-prog-000-2

#### Sqarime

Hapni terminalin dhe shkruajeni programin brenda direktorisë `prog/p000/`:
```
cd prog/p000/
nano p000.2.py
```

Krijoni edhe skedarin `input.txt` me një përmbajtje të ngjashme si kjo:
```
Ismail
Qemali
Vlora
```

Zbatojeni këtë program nga terminali duke përdorur komandën:
```
python3 p000.py < input.txt
```

Rezultati do jetë si ky:
```
Ju jeni Ismail Qemali nga Vlora.
```

Në këtë rast, me anë të shenjës `<` ne kemi *ridrejtuar* inputin e
programit, në mënyrë që të lexojë nga skedari `input.txt` dhe jo nga
tastiera.  Pra te skedari `input.txt` ne mund të vendosim gjithçka që
do ti jepnim programit nga tastiera, dhe programi do ta lexojë njëlloj
sikur ti merrte nga tastiera. Funksioni `input()` lexon nga ky skedar
një rresht të plotë deri në fund.

Këtej e tutje ne do përdorim këtë metodën me ridrejtim, sepse është më
praktike, të lejon ta shkruash inputin njëherë dhe ta testosh
programin shumë herë.  Gjithashtu lejon që vetë problemi të përcaktojë
një input shembull, bashkë me rezultatin përkatës të programit.


### Detyra

Shkruani një program që kërkon muajin dhe vitin e lindjes, dhe nxjerr në ekran:
```
Ju keni lindur në muajin <muaji> të vitit <viti>.
```
