--- 
title: "101 Problema Programimi"
author: "Dashamir Hoxha"
date: "`r Sys.Date()`"
url: http\://p101.fs.al
description: "Problema programimi për fillestarë."
favicon: "images/favicon.ico"

site: bookdown::bookdown_site
documentclass: scrbook
output:
  bookdown::gitbook:
    css: style.css
    split_by: rmd
    number_sections: FALSE
    highlight: tango
    config:
      edit: https://gitlab.com/dashohoxha/101-problema-programimi/edit/master/%s
      download: ["pdf", "epub", "tex"]
      toc:
        collapse: section
        before: |
          <li><a href="./">101 Problema Programimi</a></li>
      toolbar:
        position: static
      fontsettings:
        theme: white
        family: serif
        size: 2
      sharing:
        facebook: no
        twitter: no
        all: ['facebook', 'twitter', 'linkedin', 'google']
  bookdown::pdf_book:
    number_sections: FALSE
    latex_engine: xelatex
    keep_tex: yes
    fig_caption: FALSE
    highlight: tango
  bookdown::epub_book:
    number_sections: FALSE
    fig_caption: FALSE
---
