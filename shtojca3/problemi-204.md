### Kërkesa

https://codingcompetitions.withgoogle.com/codejam/round/00000000000516b9/0000000000134e91

### Zgjidhja

```python
### To test this program with testing_tool.py first create a pipe (fifo) file:
### $ mkfifo pipe
### Then run it like this:
### $ python3 testing_tool.py 0 < pipe | python3 program.py > pipe
### $ python3 testing_tool.py 1 < pipe | python3 program.py > pipe

import sys

def ask(i):
    sys.stderr.write(">> {}\n".format(i+1))
    print(i + 1)
    sys.stdout.flush()
    reply = input()
    sys.stderr.write("<<<<< {}\n".format(reply))
    if reply == 'N':
        sys.exit()
    return reply

def answer(ans):
    sys.stderr.write(">> {}\n".format(ans))
    print(''.join(ans))
    sys.stdout.flush()
    reply = input()
    sys.stderr.write("<<<<< {}\n".format(reply))
    if reply == 'N':
        sys.exit()

letters = ['A', 'B', 'C', 'D', 'E']

T, F = [int(i) for i in input().split()]
for t in range(T):
    M = [['' for i in range(5)] for j in range(119)]
    ans = ['', '', '', '', '']

    # ask 119 questions and find the first char of the answer
    for i in range(119):
        M[i][0] = ask(i*5)
    freq = {}
    for c in [M[i][0] for i in range(119)]:
        freq[c] = freq.get(c, 0) + 1
    for c in freq.keys():
        if freq[c] == 23:
            ans[0] = c

    # ask 23 questions and find the second char of the answer
    for i in range(119):
        if M[i][0] != ans[0]:
            continue
        M[i][1] = ask(i*5 + 1)
    freq = {}
    for c in [M[i][1] for i in range(119)]:
        freq[c] = freq.get(c, 0) + 1
    for c in freq.keys():
        if freq[c] == 5:
            ans[1] = c

    # ask 5 questions and find the third char of the answer
    for i in range(119):
        if M[i][0] != ans[0] or M[i][1] != ans[1]:
            continue
        M[i][2] = ask(i*5 + 2)
    freq = {}
    for c in [M[i][2] for i in range(119)]:
        freq[c] = freq.get(c, 0) + 1
    for c in freq.keys():
        if freq[c] == 1:
            ans[2] = c

    # ask 1 more question and find the forth char
    for i in range(119):
        if M[i][0] != ans[0] or M[i][1] != ans[1] or M[i][2] != ans[2]:
            continue
        c1 = ask(i*5 + 3)
    for c in letters:
        if c != c1 and c not in ans:
            ans[3] = c
            break

    # find the last char
    for c in letters:
        if c not in ans:
            ans[4] = c
            break

    # send the answer
    answer(ans)
```

