### Kërkesa

https://codingcompetitions.withgoogle.com/kickstart/round/0000000000051060/00000000000588f4

### Zgjidhja

```python
import sys

t = int(input())
wrong_answer = False
while t > 0 and not wrong_answer:
    t -= 1
    a, b = map(int, input().split())
    n = int(input())
    while not wrong_answer:
        g = (a + b) // 2 + 1
        print(g)
        sys.stdout.flush()
        r = input()
        if r == 'WRONG_ANSWER':
            wrong_answer = True
        elif r == 'TOO_SMALL':
            a = g
        elif r == 'TOO_BIG':
            b = g - 1
        else:
            break
```

