### Kërkesa

Pas një kohe të gjatë, Cufi vendosi më në fund ta rinovojë shtëpinë.
Shtëpia e Cufit ka **N** dhoma. Secila prej dhomave është e lyer me
një nga ngjyrat e kuqe, blu dhe jeshile.  Konfigurimi i ngjyrave të
shtëpisë na jepet me anë të një vargu me shkronjat **R**, **B**,
**G**. Cufi do që ta lyejë shtëpinë në mënyrë që të gjitha dhomat të
kenë të njëjtën ngjyrë, por ngaqë është pak dembel, do që të lyejë sa
më pak dhoma (mundësisht asnjë). Bëni një program që gjen numrin më të
vogël të dhomave që duhet të lyejë Cufi.

Referenca: https://www.codechef.com/problems/COLOR

#### Shembull

```
$ cat input.txt
3
3
RGR
3
RRR
3
RGB

$ python3 prog.py < input.txt
1
0
2
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n = int(input())
    S = input()
    r = 0
    g = 0
    b = 0
    for c in S:
        if c == 'R':
            r += 1
        elif c == 'G':
            g += 1
        else:
            b += 1
    print(n - max(r, g, b))
```

#### Sqarime

Gjejmë sa dhoma janë me secilën ngjyrë. Numri më i vogël i lyerjeve
bëhet nqs të gjitha dhomat i kthejmë në ngjyrën që kanë shumica e
dhomave.

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    S = input()
    r = S.count('R')
    g = S.count('G')
    b = S.count('B')
    print(n - max(r, g, b))
```
