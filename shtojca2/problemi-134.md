### Kërkesa

Një ari i vogël polar ka qejf të hajë biskota dhe të pijë qumësht. Për
këtë arsye shkon shpesh në kuzhinë. Ai qëndron në kuzhinë **N**
minuta, dhe çdo minutë ose ha një biskotë, ose pi qumësht. Meqenëse
biskotat janë shumë të ëmbla, prindërit e kanë këshilluar që sa herë
të hajë një biskotë, në minutën tjetër duhet të pijë qumësht.

Na jepet sa minuta ka ndenjur në kuzhinë arushi dhe çfarë ka bërë çdo
minutë: ka ngrënë biskotë apo ka pirë qumësht. Gjeni nëse arushi i ka
zbatuar ose jo këshillat e prindërve, dmth që pas ngrënies së një
biskote ka pirë qumësht.

Referenca: https://www.codechef.com/problems/COOMILK

#### Shembull

```
$ cat input.txt
4
7
cookie milk milk cookie milk cookie milk
5
cookie cookie milk milk milk
4
milk milk milk milk
1
cookie

$ python3 prog.py < input.txt
YES
NO
YES
NO
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    L = input().split()
    if L[-1] == 'cookie':
        print('NO')
    else:
        for i in range(n-1):
            if L[i] == 'cookie' and L[i+1] == 'cookie':
                print('NO')
                break
        else:
            print('YES')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    L = input().split()
    L.append('cookie')
    for i in range(n):
        if L[i] == 'cookie' and L[i+1] == 'cookie':
            print('NO')
            break
    else:
        print('YES')
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    n = int(input())
    L = input().split()
    L.append(None)
    for i in range(n):
        if L[i] == 'cookie' and L[i+1] != 'milk':
            print('NO')
            break
    else:
        print('YES')
```
