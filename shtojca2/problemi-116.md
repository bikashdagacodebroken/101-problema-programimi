### Kërkesa

Në një listë me këngë, secila prej këngëve e ka gjatësinë një numër të
plotë minutash, dhe secila këngë e ka gjatësinë të ndryshme nga të
tjerat. Kënga juaj e preferuar ndodhet në pozicionin **K**. Më pas ju
i rendisni këngët sipas gjatësisë së tyre. Në cilin pozicion ndodhet
tani kënga juaj e preferuar?

Referenca: https://www.codechef.com/problems/JOHNY

#### Shembull

```
$ cat input.txt
3
4
1 3 4 2
2
5
1 2 3 9 4
5
5
1 2 3 9 4
1

$ python3 prog.py < input.txt
3
4
1
```

1. Në rastin e parë kemi: `N = 4`, `L = [1, 3, 4, 2]`, `K = 2`. Kënga
   e preferuar është **3**.  Pas renditjes kemi `L = [1, 2, 3, 4]`,
   kështu që përgjigja është **3**.

2. Në rastin e dytë: `N = 5`, `L = [1, 2, 3, 9, 4]`, `K = 5`. Kënga e preferuar
   është **4**. Pas renditjes do jetë në pozicionin **4**.

3. Në rastin e tretë: `N = 5`, `L = [1, 2, 3, 9, 4]`, `K = 1`. Kënga e
   preferuar është **1**.  Pas renditjes do jetë prapë në pozicionin
   **1**.

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    L = list(map(int, input().split()))
    k = int(input())
    p = L[k - 1]    # kënga e preferuar
    L.sort()
    for i in range(n):
        if L[i] == p:
            break
    print(i + 1)
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    L = [0] + list(map(int, input().split()))
    k = int(input())
    p = L[k]    # kënga e preferuar
    L.sort()
    for i in range(n):
        if L[i] == p:
            break
    print(i)
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    n = int(input())
    L = [0] + list(map(int, input().split()))
    k = int(input())
    p = L[k]    # kënga e preferuar
    L.sort()
    print(L.index(p))
```
