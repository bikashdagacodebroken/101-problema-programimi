### Kërkesa

Një olimpiadë programimi është zhvilluar në një qytet në vitet `2010`,
`2015`, `2016`, `2017`, `2019`. Bëni një program që merr disa vite dhe
për secilin prej tyre nxjerr `HOSTED` nëse olimpiada është zhvilluar
atë vit në qytet, ose `NOT HOSTED` nëse nuk është zhvilluar.

Referenca: https://www.codechef.com/problems/SNCKYEAR

#### Shembull

```
$ cat input.txt
2
2019
2018

$ python3 prog.py < input.txt
HOSTED
NOT HOSTED
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    y = int(input())
    if y == 2010 or y == 2015 or y == 2016 or y == 2017 or y == 2019:
        print ('HOSTED')
    else:
        print ('NOT HOSTED')
```

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    y = int(input())
    if y in [2010, 2015, 2016, 2017, 2019]:
        print ('HOSTED')
    else:
        print ('NOT HOSTED')
```
