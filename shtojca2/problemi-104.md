### Kërkesa

Në një restorant ka filluar punë një kamarier i ri. Meqenëse nuk është
dhe aq i mirë në matematikë, ndonjëherë e kthen reston gabim. Shefi i
jep atij një problem të thjeshtë: Sa bëjnë $`a - b`$?  Përgjigja e tij
është gabim. Dhe për çudi ka vetëm një shifër gabim. E merrni dot me
mënd?

A mund të shkruani një program që bën të njëjtin gabim? Programi merr
dy numra `a` dhe `b` dhe duhet të japë një diference të gabuar `a-b`.
Përgjigja duhet të jetë një numër pozitiv që ka të njëjtin numër
shifrash me diferencën e saktë, ku të gjitha shifrat janë njësoj me
diferencën e saktë, me përjashtim të njërës që duhet të jetë e
ndryshme. Zeroja nuk duhet të jetë si shifër e parë (nga e majta).
Nëse ka përgjigje të ndryshme që i plotësojnë këto kushte, mjafton të
jepni vetëm njërën.

Referenca: https://www.codechef.com/problems/CIELAB

#### Shembull

```
$ cat input.txt
3
5858 1234
239 230
4375 4375

$ python3 prog.py < input.txt
1624
8
1
```

Në rastin e parë, diferenca e saktë është `5858 - 1234 = 4624`, kështu
që këto përgjigje do ishin të sakta: `2624`, `4324`, `4623`, `4604`,
`4629`, kurse këto do ishin të gabuara: `0624`, `624`, `5858`, `4624`,
`04624`.

### Zgjidhja

```python
T = int(input())
for t in range(T):
    a, b = map(int, input().split())
    diff = a - b
    print(diff-1 if diff % 10 == 9 else diff+1)
```

#### Sqarime

Nëse shifra e fundit e diferencës është `9` mjafton që ti zbresim
diferencës `1`, përndryshe mjafton që ti shtojmë `1`, dhe në çdo rast
do jemi të garantuar që vetëm shifra e fundit do jetë e ndryshme nga
ajo e diferencës.
