### Kërkesa

Në një listë me **N** numra natyrorë mund të shtojmë edhe **K** numra
të tjerë sipas dëshirës, dhe pastaj gjejmë të mesmen e të gjithë
numrave. E mesmja e një liste është elementi që ndodhet në mesin e
listës, pasi ti kemi renditur. P.sh. e mesmja e **[2, 1, 5, 2, 4]**
është **2**, kurse e mesmja e **[3, 3, 1, 3, 3]** është 3.

Gjeni se sa është vlera më e madhe e të mesmes që mund të merret në
këtë mënyrë. Na jepet gjithashtu që **K < N** dhe që **N + K** është
numër tek.

Referenca: https://www.codechef.com/problems/CK87MEDI

#### Shembull

```
$ cat input.txt
3
2 1
4 7
4 3
9 2 8 6
5 2
6 1 1 1 1

$ python3 prog.py < input.txt
7
9
1
```

Na jepen numrat **N** dhe **K**, dhe pastaj N numrat e listës.

Në rastin e parë, një nga zgjidhjet e mundshme është të shtojmë 9,
duke formuar listën [4, 7, 9], e mesmja e të cilës është 7.

Në rastin e tretë, sido që të jenë 2 numrat që do shtohen, e mesmja do
jetë gjithmonë 1.

### Zgjidhja

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    L = [int(i) for i in input().split()]
    L.sort()
    print(L[(n + k) // 2])
```
