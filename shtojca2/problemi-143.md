### Kërkesa

Jepet një listë me numra. A mund të formohet një palindromë me këta
numra?

Referenca: https://www.codechef.com/DARG2019/problems/TUPALIN

#### Shembull

```
$ cat input.txt
1
6
10 729 10 10 729 10

$ python3 prog.py < input.txt
YES
```

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    L = [int(i) for i in input().split()]
    F = {}
    for i in L:
        F[i] = F.get(i, 0) + 1
    odd = 0
    for i in F.keys():
        if F[i] % 2 == 1:
            odd += 1
    print('YES') if odd < 2 else print('NO')
```

#### Sqarime

Gjejmë se sa herë përsëritet çdo numër në këtë listë.  Një palindromë
mund të formohet vetëm nëse numri i përsëritjeve për çdo numër është
çift, me përjashtim të njërit që mund të jetë tek.
