### Kërkesa

Loli, vëllai i vogël i Colit, sapo ka mësuar disa prej shkronjave të
alfabetit, dhe mund të lexojë vetëm ato fjalë që përbëhen prej
shkronjave që njeh. Coli i jep atij një libër për të lexuar dhe do të
dijë se cilat prej fjalëve mund të lexojë Loli.

Referenca: https://www.codechef.com/problems/ALPHABET

#### Shembull

```
$ cat input.txt
act
2
cat
dog

$ python3 prog.py < input.txt
Yes
No
```

Rreshti i parë përmban shkronjat që ka mësuar Loli. Fjala e parë i ka
të gjitha shkronjat të njohura, fjala e dytë jo.

### Zgjidhja

```python
alphabet = input()
N = int(input())
for i in range(N):
    word = input()
    for c in word:
        if c not in alphabet:
            print('No')
            break
    else:
        print('Yes')
```
