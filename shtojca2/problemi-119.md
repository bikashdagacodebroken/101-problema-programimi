### Kërkesa

Në kohët e lashta, komandanti i një ushtrie ishte supersticioz. Ai e
quante një ushtar "me fat" nëse numri i armëve që kishte ushtari ishte
çift dhe "pa fat" në të kundërt. Ushtrinë e quante "gati për betejë"
nëse numri i ushtarëve me fat ishte më i madh se ai i ushtarëve pa
fat. Në të kundërt e quante "jo gati".

Nëse ju jepet numri i armëve që ka çdo ushtar, bëni një program që
gjen nëse ushtria është gati për betejë ose jo.

Referenca: https://www.codechef.com/problems/AMR15A

#### Shembull

```
$ cat input.txt
5
1
1
1
2
4
11 12 13 14
3
2 3 4
5
1 2 3 4 5

$ python3 prog.py < input.txt
NOT READY
READY FOR BATTLE
NOT READY
READY FOR BATTLE
NOT READY
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n = int(input())
    L = list(map(int, input().split()))
    odd = 0
    even = 0
    for a in L:
    if a % 2 == 0:
       even += 1
    else:
        odd +=1
    if even > odd:
        print('READY FOR BATTLE')
    else:
        print('NOT READY')
```

