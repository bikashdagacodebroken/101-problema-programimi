### Kërkesa

Alisa dhe Beni po bëjnë një lojë me numra. Fillimisht Alisa ka një
numër **A** kurse Beni një numër **B**. Loja ka gjithsej **N** hapa
dhe Alisa me Benin luajnë me radhë. Kur është radha e tij, secili
lojtar e shumëzon numrin e tij me dy. E para luan Alisa.

Bëni një program i cili merr numrat **A**, **B** dhe **N** dhe gjen sa
është herësi i pjesëtimit të numrave në fund të lojës (duke pjesëtuar
më të madhin me më të voglin).

Referenca: https://www.codechef.com/problems/TWONMS

#### Shembull

```
$ cat input.txt
3
1 2 1
3 2 3
3 7 2

$ python3 prog.py < input.txt
1
3
2
```

Në rastin e parë, numrat fillestarë janë (A=1, B=2) dhe loja ka vetëm
1 hap. Në këtë hap Alisa e shumëzon numrin e saj me 2, kështu që në
fund të lojës kemi (A=2, B=2) dhe herësi është 1.

Në rastin e dytë kemi (A=3, B=2) dhe N=3. Pasi luan Alisa, Beni,
Alisa, kemi (A=12, B=4), kështu që herësi është 3.

Në rastin e tretë kemi (A=3, B=7) dhe N=2. Pasi luan Alisa dhe Beni,
kemi (A=6, B=14), kështu që herësi është 2.

### Zgjidhja

```python
for _ in range(int(input())):
    a, b, n = map(int, input().split())
    if n % 2 == 1:
        a *= 2
    print(max(a,b)//min(a,b))
```

#### Sqarime

Meqenëse dyshat thjeshtohen me njëri-tjetrin gjatë pjesëtimit, mjafton
që të shumëzojmë a-në me 2 nëse n-ja është tek, dhe pastaj mund të
gjejmë herësin.
