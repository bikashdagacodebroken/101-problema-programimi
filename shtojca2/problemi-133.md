### Kërkesa

Nga rezultatet e një olimpiade mund të hamendësojmë nivelin e aftësive
për secilin pjesëmarrës. Pjesëmarrësit mund të klasifikohen bazuar në
numrin e problemave të zgjidhura në këtë mënyrë:
- 0 problema të zgjidhura: Beginner
- 1 problem të zgjidhur: Junnior Developper
- 2 problema të zgjidhura: Middle Developer
- 3 problema të zgjidhura: Senior Developer
- 4 problema të zgjidhura: Hacker
- 5 problema të zgjidhura: [Jeff
  Dean](https://www.quora.com/What-are-all-the-Jeff-Dean-facts)

Bëni një program që merr rezultatet e pjesëmarrësve dhe nxjerr
klasifikimin e tyre.

Referenca: https://www.codechef.com/problems/CCOOK

#### Shembull

```
$ cat input.txt
7
0 0 0 0 0
0 1 0 1 0
0 0 1 0 0
1 1 1 1 1
0 1 1 1 0
0 1 1 1 1
1 1 1 1 0

$ python3 prog.py < input.txt
Beginner
Middle Developer
Junior Developer
Jeff Dean
Senior Developer
Hacker
Hacker
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    P = list(map(int, input().split()))
    p = sum(P)    # or p = P.count(1)
    if p == 0:
        print('Beginner')
    elif p == 1:
        print('Junior Developer')
    elif p == 2:
        print('Middle Developer')
    elif p == 3:
        print('Senior Developer')
    elif p == 4:
        print('Hacker')
    else:
        print('Jeff Dean')
```

### Zgjidhja 2

```python
RANK = ['Beginner', 'Junior Developer', 'Middle Developer', 'Senior Developer', 'Hacker', 'Jeff Dean']
for _ in range(int(input())):
    P = list(map(int, input().split()))
    print(RANK[sum(P)])
```

