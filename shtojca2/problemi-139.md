### Kërkesa

Në një dyqan çokollatash shkon një turist. Shitësi dhe turisti nuk e
dinë gjuhën e njëri-tjetrit, por turisti tregon me dorë një lloj
çokollatash dhe i jep shitësit disa kartëmonedha. Duke ditur çmimin e
një çokollate shitësi mund ta marrë me mend se sa copë do që të blejë
turisti. Por nqs duke hequr një prej kartëmonedhave mund të blihet
përsëri i njëjti numër çokollatash, atere ky është një rast i paqartë
dhe shitësi nuk di çfarë të bëjë, kështu që ia kthen lekët mbrapsht
turistit.

Bëni një program që merr çmimin e një çokollate dhe kartëmonedhat që
jep turisti dhe gjen se sa copë çokollata do që të blejë turisti.
Nëse rasti është i paqartë programi duhet të nxjerrë -1.

Referenca: https://www.codechef.com/problems/BUYING2

#### Shembull

```
$ cat input.txt
3
4 7
10 4 8 5
1 10
12
2 10
20 50

$ python3 prog.py < input.txt
-1
1
7
```

Në rastin e parë, sasia gjithsej e parave është 27, dhe meqë çmimi
është 7, turisti mund të marrë vetëm 3 copë. Por nqs nuk do kishte dhënë
kartëmonedhën 5 përsëri do merrte 3 copë. Kështu që është rast i paqartë.

Në rastin e dytë sasia e parave është 12, çmimi 10, kështu që mund të
marrë 1 çokollatë.

Në rastin e tretë sasia e parave është 20+50, çmimi 10, mund të marrë
7 çokollata.

### Zgjidhja

```python
for _ in range(int(input())):
    n, x = map(int, input().split())
    A = list(map(int, input().split()))
    print(sum(A) // x) if (sum(A) % x < min(A)) else print(-1)
```

#### Sqarime

Nqs mbetja e parave (restoja) është më e madhe se ndonjëra nga
kartëmonedhat, kjo do të thotë që po ta heqim atë kartëmonedhë përsëri
mund të blejmë të njëjtën sasi çokollatash, kështu që rasti është i
paqartë.
