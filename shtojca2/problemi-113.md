### Kërkesa

Vogëlushja Meli i ka qejf balonat me ngjyra dhe mami i ka blerë për
ditëlindje disa balona me ngjyra të kuqe dhe blu. Mirëpo Meli do të
dëshironte ti kishte të gjitha balonat me të njëjtën ngjyrë, kështu që
mori kolorat dhe ju fut punës për ti ngjyrosur.

Bëni një program që gjen sa është numri më i vogël i balonave që duhen
ngjyrosur, për ti bërë të gjitha me të njëjtën ngjyrë.

Referenca: https://www.codechef.com/problems/CHN09

#### Shembull

```
$ cat input.txt
3
ab
bb
baaba

$ python3 prog.py < input.txt
1
0
2
```

Balonat e kuqe i kemi shënuar me **a**, blutë me **b**. Në rastin e
parë mjafton të ngjyrosim një balonë të kuqe me blu, ose një blu me të
kuqe. Në rastin e dytë të gjitha balonat kanë të njëjtën ngjyrë. Kurse
në rastin e tretë mjafton të ngjyrosim dy balonat blu me të kuqe.

### Zgjidhja 1

```python
for _ in range(int(input())):
    str = input()
    nr_a = 0
    nr_b = 0
    for c in str:
        if c == 'a':
            nr_a += 1
        else:
            nr_b += 1
    print(min(nr_a, nr_b))
```

#### Sqarime

Gjemë numrin e balonave të kuqe dhe blu, dhe pastaj printojmë minimumin e tyre.

### Zgjidhja 2

```python
for _ in range(int(input())):
    str = input()
    nr_a = 0
    for c in str:
        if c == 'a':
            nr_a += 1
    nr_b = len(s) - nr_a
    print(min(nr_a, nr_b))
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    str = input()
    print(min(str.count('a'), str.count('b')))
```
