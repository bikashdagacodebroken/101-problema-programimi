### Kërkesa

Kur pohojmë zakonisht e tundim kokën nga lart poshtë. Le ta shënojmë
me `Y`, për "yes".  Kur mohojmë e tundim kokën nga e majta në të
djathtë, rreth boshtit vertikal. Le ta shënojmë me `N` për "no".

Por indianët zakonisht përdorin një shenjë tjetër për të pohuar, duke
e tundur kokën nga e majta në të djathtë rreth boshtit para-mbrapa. Le
ta shënojmë këtë me `I`.

Një djalë vëzhgoi disa persona në një stacion treni dhe mbajti shënim
shenjat që bënin me kokë (duke përdorur shkronjat `Y`, `N` dhe `I`).
Bëni një program që shfaq nëse personi në fjalë është indian, jo
indian, ose nuk mund ta themi me siguri.

Referenca: https://www.codechef.com/problems/HEADBOB

#### Shembull

```
$ cat input.txt
3
5
NNNYY
6
NNINNI
4
NNNN

$ python3 prog.py < input.txt
NOT INDIAN
INDIAN
NOT SURE
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n = int(input())
    str = input()
    if 'I' in str:
        print('INDIAN')
    elif 'Y' in str:
        print('NOT INDIAN')
    else:
        print('NOT SURE')
```

#### Sqarime

Operatori `in` në këtë rast teston nëse një shkronjë ndodhet brenda
një vargu. P.sh:
```
$ python3
>>> 'b' in 'abc'
True
>>> 'x' in 'abc'
False
```
