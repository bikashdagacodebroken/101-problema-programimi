### Kërkesa

Numrat natyrorë vendosen në nyjet e një grafi që ka formën e shinave
të trenit, si në figurë:

![](images/Agha0OoP.png)

Bëni një program që merr dy numra dhe gjen nëse ka brinjë midis këtyre
dy nyjeve.

Referenca: https://www.codechef.com/problems/BRLADDER

#### Shembull

```
$ cat input.txt
7
1 4
4 3
5 4
10 12
1 3
999999999 1000000000
17 2384823

$ python3 prog.py < input.txt
NO
YES
NO
YES
YES
YES
NO
```

### Zgjidhja

```python
for _ in range(int(input())):
    a, b = map(int, input().split())
    if abs(a - b) > 2:
        print('NO')
    elif abs(a - b) == 2:
        print('YES')
    elif abs(a - b) == 1:
        if a // 2 == b // 2:
            print('NO')
        else:
            print('YES')
```
