### Kërkesa

Mësuesi ka ngritur disa nxënës në dërrasë dhe u ka dhënë nga një
ushtrim për të zgjidhur (klasa ka një dërrasë të gjatë kështu që
mësuesi mund të ngrejë në dërrasë shumë nxënës njëkohësisht).  Sapo
mësuesi doli një moment nga klasa, disa prej nxënësve u kthyen dhe
filluan të komunikojnë me shokun që kishin në krah, kurse të tjerët
vazhduan punën. Këtë situatë mund ta paraqesim me një varg të tillë:
`*><><><*`, ku një `*` tregon një nxënës që vazhdon punën, një `>`
tregon një nxënës që po flet me shokun në të djathtë, dhe një `<`
tregon një nxënës që po flet me shokun në të majtë. Sapo dëgjojnë
mësuesin të hyjë përsëri, nxënësit që po flisnin kthehen nga frika në
krahun e kundërt. Kur mësuesi shikon dy nxënës përball njëri-tjetrit
ai mendon se ata po flisnin, kështu që i ndëshkon të dy. Gjeni sa
dyshe nxënësish ndëshkohen prej mësuesit.

Referenca: https://www.codechef.com/problems/CHEFSTUD

#### Shembull

```
$ cat input.txt
4
><
*><*
><><
*><><><*

$ python3 prog.py < input.txt
0
0
1
2
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    S = list(input())
    for i in range(len(S)):
        if S[i] == '>':
            S[i] = '<'
        elif S[i] == '<':
            S[i] = '>'
    nr = 0
    for i in range(len(S) - 1):
        if S[i] == '>' and S[i+1] == '<':
            nr += 1
    print(nr)
```


### Zgjidhja 2

```python
for _ in range(int(input())):
    S = input()
    nr = 0
    for i in range(len(S) - 1):
        if S[i] == '<' and S[i+1] == '>':
            nr += 1
    print(nr)
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    print(input().count('<>'))
```
