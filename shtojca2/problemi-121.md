### Kërkesa

Na jepet një numër binar (i cili ka vetëm shifrat **0** dhe
**1**). Bëni një program i cili gjen nëse është e mundur që të bëhet i
barabartë numri i zerove dhe njëshave, duke ndryshuar të shumtën një
shifër (nga 1 në 0 ose nga 0 në 1).

#### Shembull

```
$ cat input.txt
4
101
11
0
10000

$ python3 prog.py < input.txt
No
Yes
No
No
```

Në rastin e parë nuk është e mundur ti bëjmë të barabarta numrat e
zerove dhe njëshave.  Në rastin e dytë mund ta bëjmë një njësh zero
dhe do kemi numër të barabartë zerosh dhe njëshash.  Në rastin e tretë
nuk mund të kemi një numër të barabartë zerosh dhe njëshash. Në rastin
e katërt gjithashtu nuk mund të bëjmë një numër të barabartë zerosh
dhe njëshash.

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    N = input()
    cnt0 = 0
    cnt1 = 0
    for d in N:
        if d == '0':
            cnt0 += 1
        else:
            cnt1 += 1
    if cnt0 == cnt1 or abs(cnt0 - cnt1) == 2:
        print('Yes')
    else:
        print('No')
```

#### Sqarime

Gjejmë numrin e zerove dhe të njëshave. Nëse janë të barabartë nuk
është nevoja të ndryshojmë gjë. Nëse diferenca midis tyre është 2, me
një ndryshim mund ti bëjmë të barabarta.

### Zgjidhja 2

```python
for _ in range(int(input())):
    N = input()
    cnt0 = N.count('0')
    cnt1 = N.count('1')
    if cnt0 == cnt1 or abs(cnt0 - cnt1) == 2:
        print('Yes')
    else:
        print('No')
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    N = input()
    cnt1 = sum(map(int, list(N)))
    cnt0 = len(N) - cnt1
    if cnt0 == cnt1 or abs(cnt0 - cnt1) == 2:
        print('Yes')
    else:
        print('No')
```
