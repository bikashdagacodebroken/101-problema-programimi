### Kërkesa

Shefi ka gjetur një libër me receta gatimesh, ku çdo gatim ka 4
përbërës.  Ai do zgjedh 2 receta për ti gatuar për drekë, por dëshiron
që këto dy gatime të mos jenë të ngjashme. Dy gatime quhen të ngjashme
nëse kanë 2 ose më shumë përbërës të njëjtë.

Bëni një program që merr përbërësit e dy gatimeve dhe gjen nëse janë
të ngjashme ose jo.

Referenca: https://www.codechef.com/problems/SIMDISH

#### Shembull

```
$ cat input.txt
5
eggs sugar flour salt
sugar eggs milk flour
aa ab ac ad
ac ad ae af
cookies sugar grass lemon
lemon meat chili wood
one two three four
one two three four
gibberish jibberish lalalalala popopopopo
jibberisz gibberisz popopopopu lalalalalu

$ python3 prog.py < input.txt
similar
similar
dissimilar
similar
dissimilar
```

### Zgjidhja 1

```python
T = int(input())
for _ in range(T):
    R1 = input().split()
    R2 = input().split()
    n = 0
    for r in R1:
        if r in R2:
            n += 1
    if n >= 2:
        print('similar')
    else:
        print('dissimilar')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    R1 = set(input().split())
    R2 = set(input().split())
    n = len(R1.intersection(R2))
    print('similar') if n >= 2 else print('dissimilar')
```

