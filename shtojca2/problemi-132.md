### Kërkesa

Ceni kishte një kuti me **N** numra në të: $`A_1, A_2, ... ,
A_n`$. Gjithashtu kishte vendosur edhe numrin **N** në krye, për të
ditur sa numra janë. Pra kutia kishte gjithsej **N+1** numra. Por nga
gëzimi i olimpiadës [IOI](http://ioi2017.org/) ai filloi të kërcente
me kutinë në xhep dhe numrat iu ngatërruan, dhe tani nuk e di më se
cili nga **N+1** numrat është numri **N** dhe cilët janë numrat e
tjerë. Atij i duhet të gjejë më të madhin e numrave të dhënë. A mund
ta ndihmoni?

Referenca: https://www.codechef.com/problems/LOSTMAX

#### Shembull

```
$ cat input.txt
3
1 2 1
3 1 2 8
1 5 1 4 3 2

$ python3 prog.py < input.txt
1
8
4
```

Në rastin e parë, N=2 dhe numrat janë {1, 1}, kështu që më i madhi është 1.

Në rastin e dytë N=3 dhe numrat janë {1, 2, 8}. Më i madhi është 8.

Në rastin e tretë N=5 dhe numrat janë {1, 1, 4, 3, 2}. Më i madhi është 4.

### Zgjidhja

```python
for _ in range(int(input())):
    A = list(map(int, input().split()))
    A.sort(reverse=True)
    n = len(A) - 1
    if A[0] > n:
        print(A[0])
    else:
        print(A[1])
```
