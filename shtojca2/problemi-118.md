### Kërkesa

Gjuhët e harruara janë gjuhë që janë përdorur dikur gjerësisht por sot
nuk përdoren më. Megjithatë disa prej fjalëve të tyre mund të jenë ende
në përdorim në gjuhët e sotme.

Ju keni gjetur në internet N fjalë të një gjuhe të harruar.
Gjithashtu keni edhe K fjali që përdoren në gjuhët e sotme. Detyra
juaj është që të përcaktoni për secilën prej N fjalëve nëse gjendet në
ndonjërën nga këto K fjalitë ose jo.

Referenca: https://www.codechef.com/problems/FRGTNLNG

#### Shembull

```
$ cat input.txt
2
3 2
piygu ezyfo rzotm
1 piygu
6 tefwz tefwz piygu ezyfo tefwz piygu
4 1
kssdy tjzhy ljzym kegqz
4 kegqz kegqz kegqz vxvyj

$ python3 prog.py < input.txt
YES YES NO
NO NO NO YES
```

Kemi 2 raste testimi. Në rastin e parë, kemi `N=3` fjalë të gjuhës së
harruar dhe `K=2` fjali të gjuhëve të sotme. Pastaj vijnë 2 fjalitë,
ku e para ka 1 fjalë dhe e dyta ka 6 fjalë. Dy fjalët e para ndodhen
në këtë fjali, kurse e treta jo.

### Zgjidhja

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    fjalet_e_vjetra = input().split()
    fjalet_e_reja = []
    for i in range(k):
        fjalet_e_reja += input().split()[1:]
    for fjale in fjalet_e_vjetra:
        if fjale in fjalet_e_reja:
            print('YES', end=' ')
        else:
            print('NO', end=' ')
    print()
```
